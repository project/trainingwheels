<?php
/**
 * @file
 * tw_course.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function tw_course_ctools_plugin_api() {
  return array("version" => "1");
}

/**
 * Implements hook_node_info().
 */
function tw_course_node_info() {
  $items = array(
    'course' => array(
      'name' => t('Course'),
      'base' => 'node_content',
      'description' => t('Training Wheels individual course.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
