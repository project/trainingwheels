<?php

use TrainingWheels\Course\DevCourse;
use TrainingWheels\Course\DrupalCourse;
use TrainingWheels\Course\NodejsCourse;
use TrainingWheels\Conn\LocalServerConn;
use TrainingWheels\Conn\SSHServerConn;
use TrainingWheels\Environment\DevEnv;
use TrainingWheels\Environment\CentosEnv;
use TrainingWheels\Environment\UbuntuEnv;

/**
 * Ensure parameters are passed as actual strings.
 */
function twcore_assert_valid_strings($function, $params) {
  if (is_string($params)) {
    $params = array($params);
  }

  if (isset($params) && is_array($params) && count($params) > 0) {
    foreach ($params as $param) {
      if (!is_string($param) || strlen($param) < 2) {
        throw new Exception("Invalid string parameter passed to $function function.");
      }
    }
  }
  else {
    throw new Exception("No parameters passed to $function function.");
  }
}

/**
 * Grab the first item from the field.
 */
function _twcore_get_item($node, $field) {
  $item = field_get_items('node', $node, $field);
  if ($item) {
    return reset($item);
  }
  else {
    return FALSE;
  }
}

/**
 * Given a node object, extract the relevant data from the fields and return
 * as a sane array.
 */
function twcore_node_fields_extract_params($node) {
  $conn = _twcore_get_item($node, 'field_course_env_conn');
  if ($conn['value'] == 'localhost') {
    $conn['host'] = 'localhost';
  }
  else {
    $conn = parse_url($conn['value']);
  }

  $course = _twcore_get_item($node, 'field_course_type');
  $env = _twcore_get_item($node, 'field_course_env');
  $repo = _twcore_get_item($node, 'field_course_repository');
  $course_name = _twcore_get_item($node, 'field_course_machine_name');

  return array(
    'course' => $course['value'],
    'env' => $env['value'],
    'repo' => $repo['value'],
    'course_name' => $course_name['value'],
    'host' => $conn['host'],
    'user' => isset($conn['user']) ? $conn['user'] : '',
    'pass' => isset($conn['pass']) ? $conn['pass'] : '',
  );
}

/**
 * Factory for creating courses.
 */
function _twcore_factory_build_course($type) {
  switch ($type) {
    case 'drupal':
      $course = new DrupalCourse();
      $course->course_type = 'drupal';
    break;

    case 'nodejs':
      $course = new NodejsCourse();
      $course->course_type = 'nodejs';
    break;

    case 'drupal-multisite':
      $course = new DrupalMultiSiteCourse();
      $course->course_type = 'drupal-multisite';
    break;

    case 'dev':
      $base_path = drupal_realpath(file_default_scheme() . '://') . '/tw';
      $course = new DevCourse($base_path);
      $course->course_type = 'dev';
    break;

    default:
      throw new Exception("Course type $type not found.");
    break;
  }
  return $course;
}

/**
 * Factory for creating environments.
 */
function _twcore_factory_build_env(&$course, $type, $host, $user, $pass) {
  switch ($type) {
    case 'ubuntu':
      if ($host == 'localhost') {
        $conn = new LocalServerConn(TRUE);
      }
      else {
        $conn = new SSHServerConn($host, 22, $user, $pass, TRUE);
        if (!$conn->connect()) {
          throw new Exception("Unable to connect/login to server $host on port 22");
        }
      }
      $course->env = new UbuntuEnv($conn);
      $course->env_type = 'ubuntu';
    break;

    case 'ubuntu-local':
      $conn = new LocalServerConn(TRUE);
      $course->env = new UbuntuEnv($conn);
      $course->env_type = 'ubuntu';
    break;

    case 'centos':
      $conn = new SSHServerConn($host, 22, $user, $pass, TRUE);
      if (!$conn->connect()) {
        throw new Exception("Unable to connect/login to server $host on port 22");
      }
      $course->env = new CentosEnv($conn);
      $course->env_type = 'centos';
    break;

    case 'dev':
      $conn = new LocalServerConn(TRUE);
      $base_path = drupal_realpath(file_default_scheme() . '://') . '/tw';
      $course->env = new DevEnv($conn, $base_path);
      $course->env_type = 'dev';
    break;

    default:
      throw new Exception("Environment type $type not found.");
    break;
  }
}

/**
 * Factory function for creating Course objects given a course node id.
 */
function twcore_factory_course_create($nid) {
  $nid = (int)$nid;
  $node = node_load($nid);
  if ($node) {
    if ($node->type == 'course') {
      $params = twcore_node_fields_extract_params($node);

      $course = _twcore_factory_build_course($params['course']);
      _twcore_factory_build_env($course, $params['env'], $params['host'], $params['user'], $params['pass']);

      $course->title = $node->title;
      $course->description = $node->body['und'][0]['safe_value'];
      $course->repo = $params['repo'];
      $course->course_name = $params['course_name'];
      $course->courseid = $nid;
      $course->uri = '/course/' . $nid;
    }
    else {
      throw new Exception("Node with id $nid is not a 'course' type node, it's a '$node->type' type.");
    }
  }
  else {
    throw new Exception("CourseID invalid: node $nid does not exist.");
  }

  return $course ? $course : FALSE;
}

/**
 * Password generator.
 */
function twcore_passwd_gen() {
  $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
  $pass = array();
  for ($i = 0; $i < 8; $i++) {
    $n = mt_rand(0, strlen($alphabet) - 1);
    $pass[$i] = $alphabet[$n];
  }
  return implode($pass);
}
