<?php

/**
 * For debugging, use debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS); in
 * the service callback.
 */

/**
 * Implements hook_services_resources().
 */
function twcore_services_resources() {
  $resources = array(
    /////////////////////////
    // Class
    /////////////////////////
    'course' => array(
      'retrieve' => array(
        'file' => array('type' => 'inc', 'module' => 'twcore', 'name' => 'twcore.services'),
        'callback' => '_twcore_course_retrieve',
        'args' => array(
          array(
            'name' => 'courseid',
            'optional' => FALSE,
            'source' => array('path' => 0),
            'type' => 'int',
            'description' => 'The nid of the course to get',
          ),
        ),
        'access callback' => '_node_resource_access',
        'access callback file' => array('type' => 'inc', 'module' => 'services', 'name' => 'resources/node_resource'),
        'access arguments' => array('view'),
        'access arguments append' => TRUE,
      ),
      'index' => array(
        'file' => array('type' => 'inc', 'module' => 'twcore', 'name' => 'twcore.services'),
        'callback' => '_twcore_course_index',
        'access callback' => '_twcore_access_callback',
        'access callback file' => array('type' => 'inc', 'module' => 'twcore', 'name' => 'twcore.services'),
      ),
    ),
    /////////////////////////
    // User
    /////////////////////////
    'user' => array(
      'create' => array(
        'file' => array('type' => 'inc', 'module' => 'twcore', 'name' => 'twcore.services'),
        'callback' => '_twcore_user_create',
        'args' => array(
          array(
            'name' => 'user',
            'optional' => FALSE,
            'default value' => FALSE,
            'source' => 'data',
            'type' => 'object',
            'description' => 'User data.',
          ),
        ),
        'access callback' => '_twcore_access_callback',
        'access callback file' => array('type' => 'inc', 'module' => 'twcore', 'name' => 'twcore.services'),
      ),
      'retrieve' => array(
        'file' => array('type' => 'inc', 'module' => 'twcore', 'name' => 'twcore.services'),
        'callback' => '_twcore_user_retrieve',
        'args' => array(
          array(
            'name' => 'userid',
            'optional' => FALSE,
            'default value' => FALSE,
            'source' => array('path' => 0),
            'type' => 'string',
            'description' => 'User id.',
          ),
        ),
        'access callback' => '_twcore_access_callback',
        'access callback file' => array('type' => 'inc', 'module' => 'twcore', 'name' => 'twcore.services'),
      ),
      'update' => array(
        'file' => array('type' => 'inc', 'module' => 'twcore', 'name' => 'twcore.services'),
        'callback' => '_twcore_user_update',
        'args' => array(
          array(
            'name' => 'userid',
            'optional' => FALSE,
            'default value' => FALSE,
            'source' => array('path' => 0),
            'type' => 'string',
            'description' => 'User id.',
          ),
          array(
            'name' => 'user',
            'optional' => FALSE,
            'default value' => FALSE,
            'source' => 'data',
            'type' => 'object',
            'description' => 'User data.',
          ),
        ),
        'access callback' => '_twcore_access_callback',
        'access callback file' => array('type' => 'inc', 'module' => 'twcore', 'name' => 'twcore.services'),
      ),
      'delete' => array(
        'file' => array('type' => 'inc', 'module' => 'twcore', 'name' => 'twcore.services'),
        'callback' => '_twcore_user_delete',
        'args' => array(
          array(
            'name' => 'userid',
            'optional' => FALSE,
            'default value' => FALSE,
            'source' => array('path' => 0),
            'type' => 'string',
            'description' => 'User id.',
          ),
        ),
        'access callback' => '_twcore_access_callback',
        'access callback file' => array('type' => 'inc', 'module' => 'twcore', 'name' => 'twcore.services'),
      ),
    ),
  );
  return $resources;
}

/**
 * Simple access callback.
 */
function _twcore_access_callback() {
  return TRUE;
}

/**
 * Helper to create a course from the id format we're using.
 */
function _twcore_services_params_process($userid) {
  $parts = explode('-', $userid);
  return array(
    'course' => twcore_factory_course_create($parts[0]),
    'user_name' => $parts[1],
  );
}

/**
 * Helper to return data taking into account debugging settings.
 */
function _twcore_services_return($data, $debug = FALSE) {
  $messages = drupal_get_messages();
  if ($debug) {
    $m = array();
    foreach ($messages as $key => $val) {
      $m[$key] = (object)$val;
    }
    if (is_array($data)) {
      $data['debug_messages'] = $m;
    }
    else if (is_object($data)) {
      $data->debug_messages = $m;
    }
  }
  return $data;
}

/**
 * Create a user.
 */
function _twcore_user_create($user) {
  if ($user == FALSE) {
    return services_error('Invalid parameters passed, check JSON formatting is strict', 406);
  }
  try {
    $course = twcore_factory_course_create($user['courseid']);
    $course->usersCreate($user['user_name']);
    $output = $course->userGet($user['user_name']);
    return _twcore_services_return($output);
  }
  catch (Exception $e) {
    return services_error($e->getMessage(), 406, _twcore_services_return(array('exception' => $e->getMessage())));
  }
}

/**
 * Retrieve a user.
 */
function _twcore_user_retrieve($userid) {
  try {
    $params = _twcore_services_params_process($userid);
    $output = $params['course']->userGet($params['user_name']);
    return _twcore_services_return($output);
  }
  catch (Exception $e) {
    return services_error($e->getMessage(), 406, array('messages' => $e->getMessage()));
  }
}

/**
 * Update a user or trigger an action on a user, such as resource sync.
 */
function _twcore_user_update($userid, $user) {
  if ($userid == FALSE || $user == FALSE) {
    return services_error('Invalid parameters passed, check JSON formatting is strict', 406);
  }
  try {
    $params = _twcore_services_params_process($userid);

    if (isset($user['action']) && isset($user['resources'])) {
      if ($user['action'] == 'resources-sync') {
        if (isset($user['sync_from'])) {
          $params['course']->usersResourcesSync($user['sync_from'], $params['user_name'], $user['sync_resources']);
        }
      }
      else if ($user['action'] == 'resources-create') {
        $params['course']->usersResourcesCreate($params['user_name'], $user['sync_resources']);
      }
    }
    $output = $params['course']->userGet($params['user_name']);
    $output['result'] = $user['action'];
    return _twcore_services_return($output);
  }
  catch (Exception $e) {
    return services_error($e->getMessage(), 406, array('messages' => $e->getMessage()));
  }
}

/**
 * Delete users.
 */
function _twcore_user_delete($userid) {
  try {
    $params = _twcore_services_params_process($userid);
    $params['course']->usersDelete($params['user_name']);
    return _twcore_services_return(array('result' => 'deleted'));
  }
  catch (Exception $e) {
    return services_error($e->getMessage(), 406, array('messages' => $e->getMessage()));
  }
}

/**
 * Retrieve a course.
 */
function _twcore_course_retrieve($courseid) {
  try {
    $course = twcore_factory_course_create($courseid);
    $users = $course->usersGet('*');
    unset($course->env);
    $course->users = array_values($users);
    return _twcore_services_return($course);
  }
  catch (Exception $e) {
    return services_error($e->getMessage(), 406, array('messages' => $e->getMessage()));
  }
}

/**
 * Index of courses.
 */
function _twcore_course_index() {
  try {
    // TODO: Make this secure.
    $sql = "SELECT nid FROM {node} WHERE type = 'course'";
    $nids = db_query($sql)->fetchCol();
    $courses = array();
    foreach ($nids as $courseid) {
      $course = twcore_factory_course_create($courseid);
      unset($course->env);
      $courses[] = $course;
    }
    return _twcore_services_return($courses, FALSE);
  }
  catch (Exception $e) {
    return services_error($e->getMessage(), 406, array('messages' => $e->getMessage()));
  }
}
