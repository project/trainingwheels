<?php

require_once('twcore_bootstrap.inc');
require_once('twcore.inc');

/**
 * Implements hook_menu().
 */
function twcore_menu() {
  $items['admin/settings/trainingwheels'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('twcore_settings_form'),
    'title' => 'Configure Training Wheels',
    'description' => 'Global settings for Training Wheels',
    'access arguments' => array('access content'),
  );
  // We need a callback per URL supported by the main JS application. They
  // can all just route to the same main app call.
  $items['trainingwheels'] = array(
    'page callback' => 'twcore_main',
    'title' => 'Training Wheels',
    'access arguments' => array('access content'),
  );
  $items['course/%'] = array(
    'page callback' => 'twcore_main',
    'title' => 'Training Wheels',
    'access arguments' => array('access content'),
  );

  return $items;
}

/**
 * The main application page.
 */
function twcore_main() {
  // Required libraries.
  libraries_load('underscore');
  libraries_load('backbone');
  libraries_load('backbone-relational');
  libraries_load('handlebars');
  libraries_load('spin');

  // Add templates to page.
  twcore_add_template('course-summary-tpl', theme('tw_course_summary_template'));
  twcore_add_template('course-tpl', theme('tw_course_template'));
  twcore_add_template('app-tpl', theme('tw_app_template'));
  twcore_add_template('user-tpl', theme('tw_user_template'));
  twcore_add_template('spinner-tpl', theme('tw_spinner_template'));

  // Add app js and css.
  drupal_add_js(drupal_get_path('module', 'twcore') . '/js/twcore_app.js');
  drupal_add_js(drupal_get_path('module', 'twcore') . '/js/handlebars_helpers.js');
  drupal_add_js(drupal_get_path('module', 'twcore') . '/js/jquery.spin.js');
  drupal_add_css(drupal_get_path('module', 'twcore') . '/css/twcore.css');

  // Return a container div for the app to anchor itself to.
  return array(
    '#type' => 'html_tag',
    '#tag' => 'div',
    '#attributes' => array(
      'id' => 'tw-app',
    ),
  );
}

/**
 * Implements hook_theme().
 */
function twcore_theme($existing, $type, $theme, $path) {
  return array(
    'tw_user_template' => array(
      'template' => 'user',
      'path' => drupal_get_path('module','twcore') . '/tpl',
      'variables' => array(),
    ),
    'tw_course_summary_template' => array(
      'template' => 'course-summary',
      'path' => drupal_get_path('module','twcore') . '/tpl',
      'variables' => array(),
    ),
    'tw_course_template' => array(
      'template' => 'course',
      'path' => drupal_get_path('module','twcore') . '/tpl',
      'variables' => array(),
    ),
    'tw_app_template' => array(
      'template' => 'app',
      'path' => drupal_get_path('module','twcore') . '/tpl',
      'variables' => array(),
    ),
    'tw_spinner_template' => array(
      'template' => 'spinner',
      'path' => drupal_get_path('module','twcore') . '/tpl',
      'variables' => array(),
    ),
    'handlebars_template' => array(
      'render element' => 'template',
    ),
  );
}

/**
 * Contains the settings that are used to configure Training Wheels.
 */
function twcore_settings_form() {
  $form['placeholder'] = array(
    '#type' => 'textfield',
    '#title' => 'Do you love Training Wheels?',
    '#description' => 'Answer truthfully.',
    '#default_value' => variable_get('placeholder', 'Yes!'),
  );
  return system_settings_form($form);
}

/**
 * Implements hook_libraries_info().
 */
function twcore_libraries_info() {
  $libraries['underscore'] = array(
    'name' => 'Underscore.js',
    'vendor url' => 'http://documentcloud.github.com/underscore/',
    'download url' => 'http://documentcloud.github.com/underscore/',
    'version arguments' => array(
      'file' => 'underscore-min.js',
      'pattern' => '@Underscore\.js ([0-9\.]+)$@',
      'lines' => 1,
    ),
    'files' => array(
      'js' => array(
        'underscore-min.js'
      ),
    ),
  );

  $libraries['backbone'] = array(
    'name' => 'Backbone.js',
    'vendor url' => 'http://documentcloud.github.com/backbone/',
    'download url' => 'http://documentcloud.github.com/backbone/',
    'version arguments' => array(
      'file' => 'backbone.js',
      'pattern' => '@Backbone\.js ([0-9\.]+)$@',
      'lines' => 1,
    ),
    'files' => array(
      'js' => array(
        'backbone.js'
      ),
    ),
    'variants' => array(
      'minified' => array(
        'files' => array(
          'js' => array(
            'backbone-min.js',
          )
        )
      )
    )
  );

  $libraries['backbone-relational'] = array(
    'name' => 'Backbone-relational.js',
    'vendor url' => 'https://github.com/PaulUithol/Backbone-relational',
    'download url' => 'https://github.com/PaulUithol/Backbone-relational',
    'version arguments' => array(
      'file' => 'backbone-relational.js',
      'pattern' => '@Backbone-relational\.js ([0-9\.]+)$@',
      'lines' => 2,
    ),
    'files' => array(
      'js' => array(
        'backbone-relational.js'
      ),
    ),
  );

  $libraries['handlebars'] = array(
    'name' => 'Handlebars.js',
    'vendor url' => 'https://github.com/wycats/handlebars.js/',
    'download url' => 'https://github.com/wycats/handlebars.js/',
    'version arguments' => array(
      'file' => 'handlebars-1.0.0.beta.6.js',
      'pattern' => '@Handlebars\.VERSION = "([0-9a-z\.]+)";@',
      'lines' => 4,
    ),
    'files' => array(
      'js' => array(
        'handlebars-1.0.0.beta.6.js'
      ),
    ),
  );

  $libraries['spin'] = array(
    'name' => 'Spin.js',
    'vendor url' => 'http://fgnass.github.com/spin.js/',
    'download url' => 'http://fgnass.github.com/spin.js/',
    'version arguments' => array(
      'file' => 'spin.js',
      'pattern' => '@spin\.js\#v([0-9\.]+)$@',
      'lines' => 1,
    ),
    'files' => array(
      'js' => array(
        'spin.js'
      ),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_page_build().
 */
function twcore_page_build(&$page) {
  $templates = twcore_get_templates();
  foreach ((array) $templates as $template_id => $template_source) {
    $page['page_bottom']['handlebars_templates'][] = array(
      '#theme' => 'handlebars_template',
      '#template_id' => $template_id,
      '#template_source' => $template_source,
    );
  }
}

/**
 * Theme function for wrapping the template as needed for Backbone.
 */
function theme_handlebars_template($variables = array()) {
  $template_id = $variables['template']['#template_id'];
  $template_source = $variables['template']['#template_source'];
  return <<<TEMPLATE
<script id="{$template_id}" type="text/x-handlebars-template">
{$template_source}
</script>
TEMPLATE;
}

/**
 * Add template to array of templates to be included in page.
 */
function twcore_add_template($template_id, $template_source) {
  global $_trainingwheels_templates;
  $_trainingwheels_templates[$template_id] = $template_source;
}

/**
 * Get the tempalates array.
 */
function twcore_get_templates() {
  global $_trainingwheels_templates;
  return $_trainingwheels_templates;
}