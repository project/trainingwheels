=======================
== Goals
=======================

1. Simple yet well architected web interface that uses modern tech like backbone
and REST calls to TrainingWheels, which then controls the server.

2. A class will be a node with CCK text fields. Possible fields:

- Class name
- Instructor credentials
- Students associated with this class.
- Repository url
- Name of environment type (e.g. Drupal)
- Environment settings - connection info

=======================
== Server Preparation
=======================

1. MySQL

You need a .my.cnf file in your root user's home directory. It should look like
this:

[client]
user=root
pass=<password>

This enables Training Wheels to connect to the MySQL server and perform admin by
just executing the 'mysql' command directly.

2. Skel

Add /skel directories to your server. They must be in /opt/trainingwheels/skel.

Name them skel_instructor and skel_student.