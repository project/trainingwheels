<div class="course-view">
  <div class="course-info">
    <h2>{{title}}</h2>
    <div>
      {{{description}}}
    </div>
    <h2>Machine name</h2>
    <div>
      {{course_name}}
    </div>
  </div>
  <div class="course-user-info">
    <h2>Users</h2>
    <h3>Add user</h3>
    <form action="#" name="tw-add-user-form" onSubmit="return false;">
      <div>
        <input type="textfield" name="username" id="add-username" />
        <input type="submit" value="add" />
      </div>
    </form>
    <div id="user-list"></div>
  </div>
</div>