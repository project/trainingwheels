<?php

use Symfony\Component\ClassLoader\UniversalClassLoader;
use Symfony\Component\ClassLoader\ApcUniversalClassLoader;

/**
 * Symfony component that will be in Drupal 8, for PSR-0 style loading of
 * classes. This is code stolen from Drupal 8's drupal_classloader().
 */
function twcore_classloader() {
  // By default, use the UniversalClassLoader which is best for development,
  // as it does not break when code is moved on the file system. However, as it
  // is slow, allow to use the APC class loader in production.
  static $loader;

  if (!isset($loader)) {
    $path = DRUPAL_ROOT . '/profiles/trainingwheels/libraries';

    // Include the Symfony ClassLoader for loading PSR-0-compatible classes.
    require_once $path . '/Symfony/Component/ClassLoader/UniversalClassLoader.php';

    // @todo Use a cleaner way than variable_get() to switch autoloaders.
    switch (variable_get('autoloader_mode', 'default')) {
      case 'apc':
        if (function_exists('apc_store')) {
          require_once $path . '/Symfony/Component/ClassLoader/ApcUniversalClassLoader.php';
          $loader = new ApcUniversalClassLoader('twcore.' . $GLOBALS['drupal_hash_salt']);
          break;
        }
      // Fall through to the default loader if APC was not loaded, so that the
      // site does not fail completely.
      case 'dev':
      case 'default':
      default:
        $loader = new UniversalClassLoader();
        break;
    }

    // Register explicit namespaces.
    $loader->registerNamespaces(array(
      'TrainingWheels' => DRUPAL_ROOT . '/' . drupal_get_path('module', 'twcore') .  '/lib',
    ));

    // Register the loader with PHP.
    $loader->register();
  }
  return $loader;
}

twcore_classloader();