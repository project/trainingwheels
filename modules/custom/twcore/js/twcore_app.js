(function($) {
  Drupal.behaviors.trainingwheels = {
    // ## attach()
    //
    // We start the app in a Drupal.behaviors attach function, this way we can
    // be sure that our settings and required libraries have already been
    // loaded.
    attach: function() {

      Drupal.tw = {};

      /**
       * Basic template containing a spinner.
       */
      Drupal.tw.spinnerTemplate = Handlebars.compile($('#spinner-tpl').html());

      /**
       * User model.
       */
      Drupal.tw.UserModel = Backbone.RelationalModel.extend({
        urlRoot : "/tw/rest/user",
        idAttribute : 'userid'
      });

      /**
       * Course model.
       */
      Drupal.tw.CourseModel = Backbone.RelationalModel.extend({
        urlRoot : "/tw/rest/course",
        idAttribute : "courseid",
        relations : [{
          type : Backbone.HasMany,
          key : 'users',
          relatedModel : Drupal.tw.UserModel,
          reverseRelation : {
            key : 'courseid',
            includeInJSON : 'courseid'
          }
        }]
      });

      /**
       * Courses collection.
       */
      Drupal.tw.CourseCollection = Backbone.Collection.extend({
        url: '/tw/rest/course',
        model: Drupal.tw.CourseModel
      });

      /**
       * User view.
       */
      Drupal.tw.UserView = Backbone.View.extend({
        tagName: 'div',
        className: 'user',

        template: Handlebars.compile($('#user-tpl').html()),

        events: {
          'submit form[name=tw-delete-user-form]': 'deleteUserSubmit',
          'submit form[name=tw-sync-user-form]': 'syncResourcesUserSubmit',
          'submit form[name=tw-refresh-user-form]': 'refreshUserSubmit'
        },

        initialize: function() {
          _.bindAll(this, 'render', 'deleteUserSubmit', 'destroy', 'sync', 'syncResourcesUserSubmit', 'refreshUserSubmit');
          this.model.bind('change', this.render);
          this.model.bind('reset', this.render);
          this.model.bind('destroy', this.destroy);
          this.model.bind('sync', this.sync);
        },

        render: function() {
          var json = this.model.toJSON();
          var tpl = '';
          if (typeof this.model.get('userid') == 'undefined') {
            tpl = Drupal.tw.spinnerTemplate();
          }
          else {
            tpl = this.template(json);
            this.$('.twspin').spin(false);
          }
          return this.$el.html(tpl);
        },

        deleteUserSubmit: function() {
          this.model.destroy();
        },

        destroy: function() {
          this.$el.spin('large');
        },

        sync: function(model, resp, options) {
          if (resp.result == 'deleted') {
            this.unbind();
            this.remove();
          }
          else if (resp.result == 'resources-sync') {
            this.model.unset('result', {silent: true});
            this.model.unset('action', {silent: true});
            this.model.unset('sync_from', {silent: true});
          }
          this.$el.spin(false);
        },

        syncResourcesUserSubmit: function() {
          var options = {
            action: 'resources-sync',
            sync_from: 'instructor',
            sync_resources: '*'
          };
          this.$el.spin('large');
          this.model.save(options, {wait: true});
        },

        refreshUserSubmit: function() {
          this.model.fetch();
          this.$el.spin('large');
        }
      });

      /**
       * Course summary view.
       */
      Drupal.tw.CourseSummaryView = Backbone.View.extend({
        tagName: 'div',
        className: 'course-summary',

        template: Handlebars.compile($('#course-summary-tpl').html()),

        events: {
          'click': 'summaryClick'
        },

        initialize: function() {
          _.bindAll(this, 'render', 'summaryClick');
          this.model.bind('change', this.render);
          this.model.bind('reset', this.render);
        },

        render: function() {
          return this.$el.html(this.template(this.model.toJSON()));
        },

        summaryClick: function() {
          this.$el.html('');
          this.$el.spin('large');
          Drupal.tw.app.navigate('course/' + this.model.get('courseid'), {trigger: true});
        }
      });

      /**
       * Course view.
       */
      Drupal.tw.CourseView = Backbone.View.extend({
        el: $('#tw-app'),
        template: Handlebars.compile($('#course-tpl').html()),

        events: {
          'submit form[name=tw-add-user-form]': 'newUserSubmit'
        },

        initialize: function() {
          _.bindAll(this, 'render', 'addUser', 'newUserSubmit');
          this.model.bind('change', this.render);
          this.model.bind('reset', this.render);
          this.model.bind('add:users', this.addUser);
        },

        render: function() {
            return this.$el.html(this.template(this.model.toJSON()));
        },

        addUser: function(user) {
          var userView = new Drupal.tw.UserView({model: user});
          this.$('#user-list').prepend($(userView.render()));
          this.$('.twspin').spin('large');
        },

        newUserSubmit: function() {
          var user_name = this.$('#add-username').val();
          var userModel = new Drupal.tw.UserModel({user_name: user_name, courseid: this.model});
          userModel.save();
        }
      });

      /**
       * App view.
       */
      Drupal.tw.AppView = Backbone.View.extend({
        el: $('#tw-app'),
        template: Handlebars.compile($('#app-tpl').html()),

        initialize: function() {
          _.bindAll(this, 'render', 'renderCourseSummary');
          this.model.bind('change', this.render);
          this.model.bind('reset', this.render);
          this.render();
        },

        render: function() {
          $('#tw-app').spin(false);
          this.$el.html(this.template());
          this.model.forEach(this.renderCourseSummary);
          return this.$el.html();
        },

        renderCourseSummary: function(course_model) {
          var courseSummaryView = new Drupal.tw.CourseSummaryView({model: course_model});
          this.$('#tw-course-list').append($(courseSummaryView.render()));
        }
      });

      /**
       * Router.
       */
      Drupal.tw.Router = Backbone.Router.extend({
        routes: {
          '' : 'main',
          'course/:courseid' : 'course_page'
        },

        main: function() {
          var courseCollection = new Drupal.tw.CourseCollection();
          var appView = new Drupal.tw.AppView({model: courseCollection});
          courseCollection.fetch();
        },

        course_page: function(courseid) {
          var courseModel = new Drupal.tw.CourseModel({courseid : courseid});
          var courseView = new Drupal.tw.CourseView({model: courseModel});
          courseModel.fetch();
        }
      });

      /**
       * Application.
       */
      Drupal.tw.app = null;
      Drupal.tw.bootstrap = function() {
        Drupal.tw.app = new Drupal.tw.Router();
        Backbone.history.start({pushState: true});
      }

      Drupal.tw.bootstrap();
      $('#tw-app').spin('large');
    },

    unattach: function() {
      $('#tw-app').html('');
    }
  };
})(jQuery);
