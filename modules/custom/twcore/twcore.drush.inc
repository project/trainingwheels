<?php

/**
 * @file
 * Drush commands for the Training Wheels module. These mostly need to be run
 * as root (or sudo as root).
 */

/**
 * Implements hook_drush_command().
 */
function twcore_drush_command() {
  $items['tw-user-create'] = array(
    'description' => 'Create user(s)',
    'aliases' => array('twuc'),
    'arguments' => array(
      'courseid' => 'Course id',
      'names' => 'The user names to create.',
    ),
  );
  $items['tw-user-delete'] = array(
    'description' => 'Delete user(s).',
    'aliases' => array('twud'),
    'arguments' => array(
      'courseid' => 'Course id',
      'names' => 'The user names to delete.',
    ),
  );
  $items['tw-user-retrieve'] = array(
    'description' => 'Retrieve info on user(s).',
    'aliases' => array('twur'),
    'arguments' => array(
      'courseid' => 'Course id',
      'names' => 'The user names to retrieve.',
    ),
  );
  $items['tw-resource-create'] = array(
    'description' => 'Create resources for users.',
    'aliases' => array('twrc'),
    'arguments' => array(
      'courseid' => 'Course id',
      'names' => 'The user names.',
      'resources' => 'The resource names',
    ),
  );
  $items['tw-resource-delete'] = array(
    'description' => 'Delete resources.',
    'aliases' => array('twrd'),
    'arguments' => array(
      'courseid' => 'Course id',
      'source_user' => 'The user',
      'resources' => 'The resource names',
    ),
  );
  $items['tw-resource-sync'] = array(
    'description' => 'Sync resources.',
    'aliases' => array('twrs'),
    'arguments' => array(
      'courseid' => 'Course id',
      'source_user' => 'The source user to sync from',
      'target_users' => 'The target users to sync to',
      'resources' => 'The resource names',
    ),
  );
  $items['debug'] = array(
    'description' => 'Debug.',
    'aliases' => array('d'),
    'arguments' => array(
      'courseid' => 'Course id',
    ),
  );
  return $items;
}

/**
 * Command callback.
 *
 * Create user(s).
 *
 * drush twuc 1 instructor,cilantro,basil,tumeric
 *
 */
function drush_twcore_tw_user_create($courseid, $user_names) {
  try {
    $course = twcore_factory_course_create($courseid);
    $course->usersCreate(explode(',', $user_names));
    drush_print_r('');
  }
  catch (Exception $e) {
    drush_print($e->getMessage());
  }
}

/**
 * Command callback.
 *
 * Delete user(s).
 *
 * drush twud 1 cilantro,basil,tumeric
 */
function drush_twcore_tw_user_delete($courseid, $user_names) {
  try {
    $course = twcore_factory_course_create($courseid);
    $course->usersDelete(explode(',', $user_names));
    drush_print_r('');
  }
  catch (Exception $e) {
    drush_print($e->getMessage());
  }
}

/**
 * Command callback.
 *
 * Retrieve user(s).
 *
 * drush twur 1 cilantro,basil,tumeric
 * drush twur 1 all
 *
 * We can't use '*' from the command line for drush, since bash uses this as
 * a keyword (all files and folders).
 */
function drush_twcore_tw_user_retrieve($courseid, $user_names) {
  try {
    $course = twcore_factory_course_create($courseid);
    if ($user_names != 'all') {
      $user_names = explode(',', $user_names);
    }
    else {
      $user_names = '*';
    }
    $users = $course->usersGet($user_names);
    drush_print_r($users);
  }
  catch (Exception $e) {
    drush_print($e->getMessage());
  }
}

/**
 * Command callback.
 *
 * Create resources for a user.
 *
 * drush twrc 1 instructor all
 *
 */
function drush_twcore_tw_resource_create($courseid, $user_names, $resources = 'all') {
  try {
    if (empty($resources)) {
      $resources = '*';
    }
    $resources = $resources == 'all' ? '*' : $resources;
    $course = twcore_factory_course_create($courseid);
    if ($resources != '*') {
      $resources = explode(',', $resources);
    }
    $out = $course->usersResourcesCreate(explode(',', $user_names), $resources);
    drush_print_r($out);
  }
  catch (Exception $e) {
    drush_print($e->getMessage());
  }
}

/**
 * Command callback.
 *
 * Delete resources for a user.
 *
 * drush twrd 1 instructor all
 *
 */
function drush_twcore_tw_resource_delete($courseid, $user_names, $resources = 'all') {
  try {
    if (empty($resources)) {
      $resources = '*';
    }
    $resources = $resources == 'all' ? '*' : $resources;
    $course = twcore_factory_course_create($courseid);
    if ($resources != '*') {
      $resources = explode(',', $resources);
    }
    $out = $course->usersResourcesDelete(explode(',', $user_names), $resources);
    drush_print_r($out);
  }
  catch (Exception $e) {
    drush_print($e->getMessage());
  }
}

/**
 * Command callback.
 *
 * Sync resources from one user to another.
 *
 * drush twrs 1 instructor cilantro,basil,tumeric all
 * drush twrs 1 instructor basil mysqldb
 */
function drush_twcore_tw_resource_sync($courseid, $source_user, $target_users, $resources = 'all') {
  try {
    if (empty($resources)) {
      $resources = '*';
    }
    $resources = $resources == 'all' ? '*' : $resources;
    $course = twcore_factory_course_create($courseid);
    if ($resources != '*') {
      $resources = explode(',', $resources);
    }
    $out = $course->usersResourcesSync($source_user, explode(',', $target_users), $resources);
    drush_print_r($out);
  }
  catch (Exception $e) {
    drush_print($e->getMessage());
  }
}

/**
 * Command callback.
 *
 * Debugging.
 *
 */
function drush_twcore_debug($courseid) {
  try {
    $course = twcore_factory_course_create($courseid);
    drush_print_r($course->env->fileAppendText('/tmp/mark', 'testingtext'));
  }
  catch (Exception $e) {
    drush_print($e->getMessage());
  }
}
