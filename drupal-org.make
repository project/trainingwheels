api = 2
core = 7.x

;;;
; Projects
;;;
projects[module_filter][version] = 1.7
projects[module_filter][type] = module
projects[module_filter][subdir] = contrib

projects[admin_menu][version] = 3.0-rc3
projects[admin_menu][type] = module
projects[admin_menu][subdir] = contrib

projects[ctools][version] = 1.2
projects[ctools][type] = module
projects[ctools][subdir] = contrib

projects[features][version] = 1.0
projects[features][subdir] = contrib
projects[features][type] = module

projects[strongarm][version] = 2.0
projects[strongarm][subdir] = contrib
projects[strongarm][type] = module

projects[services][version] = 3.2
projects[services][type] = module
projects[services][subdir] = contrib
projects[services][patch][] = http://drupal.org/files/services-namespace_users-1551172-3.patch

projects[jquery_update][version] = 2.2
projects[jquery_update][type] = module
projects[jquery_update][subdir] = contrib

projects[libraries][version] = 2.0
projects[libraries][type] = module
projects[libraries][subdir] = contrib

projects[devel][version] = 1.3
projects[devel][subdir] = contrib
projects[devel][type] = module

;;;
; Libraries
;;;
libraries[phpseclib][download][type] = get
libraries[phpseclib][download][url] = http://downloads.sourceforge.net/project/phpseclib/phpseclib0.2.2.zip

libraries[backbone][download][type] = get
libraries[backbone][download][url] = https://github.com/documentcloud/backbone/tarball/0.9.2

libraries[backbone-relational][download][type] = get
libraries[backbone-relational][download][url] = https://github.com/PaulUithol/Backbone-relational/tarball/0.5.0

libraries[spin][download][type] = git
libraries[spin][download][url] = https://github.com/fgnass/spin.js.git
libraries[spin][download][revision] = 63b791dd1e7139694f1caf02726dab1ac33e0177

libraries[handlebars][download][type] = get
libraries[handlebars][download][url] = https://github.com/downloads/wycats/handlebars.js/handlebars-1.0.0.beta.6.js

libraries[ClassLoader][download][type] = get
libraries[ClassLoader][download][url] = https://github.com/symfony/ClassLoader/tarball/v2.0.15
libraries[ClassLoader][subdir] = Symfony/Component

libraries[underscore][download][type] = get
libraries[underscore][download][url] = http://documentcloud.github.com/underscore/underscore-min.js
