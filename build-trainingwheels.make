api = 2
core = 7.x

projects[drupal][version] = 7.14

projects[trainingwheels][version] = 7.x-1.x
projects[trainingwheels][type] = profile
projects[trainingwheels][download][type] = git
projects[trainingwheels][download][url] = http://git.drupal.org/project/trainingwheels.git
projects[trainingwheels][download][branch] = 7.x-1.x
